import { defineStore } from "pinia";
import { ref, computed } from "vue";

export const useloginStore = defineStore("login", () => {
  const loginName = ref("");
  const islogin = computed(() => {
    return loginName.value !== "";
  });

  const login = (userName: string): void => {
    loginName.value = userName;
    localStorage.setItem("loginName", userName);
  };

  const logout = () => {
    loginName.value = " ";
    localStorage.removeItem("loginName");
  };

  const loadData = () => {
    loginName.value = localStorage.getItem("loginName") || "";
  };

  return { loginName, islogin, login, logout, loadData };
});
